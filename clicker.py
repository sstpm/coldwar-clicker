import pygame
import sys
import os
from pygame.locals import *

WINDOWHEIGHT = 600
WINDOWWIDTH = 800

#         R    G    B
BLACK = (0,    0,    0)
WHITE = (255, 255, 255)
GREEN = (0,   204,   0)
RED =   (255,   0,   0)
BLUE =  (0,   0,   255)

BGCOLOR = BLACK
TEXTCOLOR = WHITE
TILECOLOR = GREEN
AWINCOLOR = BLUE
UWINCOLOR = RED
FONTSIZE = 24
ROUND1INC = 20
ROUND2INC = 10
ROUND3INC = 7
ROUND4INC = 5
ROUND5INC = 4

BGSPRITE = pygame.image.load(os.path.join("assets", "background.png"))
BGRECT = BGSPRITE.get_rect()

# Other Sprites
AMSPRITE = pygame.image.load(os.path.join("assets", "amflag.png"))
AMRECT = AMSPRITE.get_rect()
SOVSPRITE = pygame.image.load(os.path.join("assets", "ussrflag.png"))
SOVRECT = SOVSPRITE.get_rect()
SPUTNIK = pygame.image.load(os.path.join("assets", "sputnik.png"))
SPUTRECT = SPUTNIK.get_rect(center=(WINDOWWIDTH/2, WINDOWHEIGHT/2))
DOG = pygame.image.load(os.path.join("assets", "dog.png"))
DOGRECT = DOG.get_rect(center=(WINDOWWIDTH/2, WINDOWHEIGHT/2))
RETURN = pygame.image.load(os.path.join("assets", "return.png"))
RETURNRECT = RETURN.get_rect(center=(WINDOWWIDTH/2, WINDOWHEIGHT/2))
MAN = pygame.image.load(os.path.join("assets", "man.png"))
MANRECT = MAN.get_rect(center=(WINDOWWIDTH/2, WINDOWHEIGHT/2))
MOON = pygame.image.load(os.path.join("assets", "moon.png"))
MOONRECT = MOON.get_rect(center=(WINDOWWIDTH/2, WINDOWHEIGHT/2))

# American and USSR Progress Bars, Markers, and Rects
PROGRESSBAR = pygame.image.load(os.path.join("assets", "progressbar.png"))
MARKER = pygame.image.load(os.path.join("assets", "redbar.jpg"))
ITAMARKRECT = [WINDOWWIDTH/2 - 212, WINDOWHEIGHT/2 - 200]
AMARKRECT = MARKER.get_rect(center=(ITAMARKRECT[0], ITAMARKRECT[1]))
ITUMARKRECT = [WINDOWWIDTH/2 - 212, WINDOWHEIGHT/2 + 200]
UMARKRECT = MARKER.get_rect(center=(ITUMARKRECT[0], ITUMARKRECT[1]))
APROGRECT = PROGRESSBAR.get_rect(center=(WINDOWWIDTH/2, WINDOWHEIGHT/2 - 200))
UPROGRECT = PROGRESSBAR.get_rect(center=(WINDOWWIDTH/2, WINDOWHEIGHT/2 + 200))

# Explinations
SPUTNIKEXPL = pygame.image.load(os.path.join("assets", "sputnikexpl.png"))
SPUTNIKEXPLRECT = SPUTNIKEXPL.get_rect()

GAGARINEXPL = pygame.image.load(os.path.join("assets", "maninspace-expl.png"))
GAGARINRECT = GAGARINEXPL.get_rect()

MOONEXPL = pygame.image.load(os.path.join("assets", "moonlandingexpl.png"))
MOONEXPLRECT = MOONEXPL.get_rect()

LIVING = pygame.image.load(os.path.join("assets", "livingfromspace.png"))
LIVINGRECT = LIVING.get_rect()

LAIKA = pygame.image.load(os.path.join("assets", "laika.png"))
LAIKARECT = LAIKA.get_rect()

# Define where the text for the rounds will appear during the main game loop.
TEXTOP = (WINDOWWIDTH/2, WINDOWHEIGHT/2 - 100)
TEXTBOT = (WINDOWWIDTH/2, WINDOWHEIGHT/2 + 100)

def main():
    global SURFACE, FONTSIZE, CLICKER_AMERICA, CLICKER_SOVIET, ROUND, \
           RESET_RECT, CONTINUE_RECT, RESET_SURF, CONTINUE_SURF, MULTIPLIER, FONT, \
           WINREQUIREMENT

    pygame.init()
    SURFACE = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT))
    FONT = pygame.font.SysFont("timesnewroman", FONTSIZE)

    pygame.event.clear()

    showTitleScreen()

    CLICKER_SOVIET = 0
    CLICKER_AMERICA = 0
    MULTIPLIER = 1
    ROUND = 1
    WINREQUIREMENT = updateWinrequirement(0)

    while True:
        checkForQuit(False)


        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if (event.key == K_a):
                    CLICKER_AMERICA += 1

                    if (ROUND == 1):
                        ITAMARKRECT[0] += ROUND1INC
                        global AMARKRECT
                        AMARKRECT = MARKER.get_rect(center=(ITAMARKRECT[0], ITAMARKRECT[1]))
                    elif (ROUND == 2):
                        ITAMARKRECT[0] += ROUND2INC
                        global AMARKRECT
                        AMARKRECT = MARKER.get_rect(center=(ITAMARKRECT[0], ITAMARKRECT[1]))
                    elif (ROUND == 3):
                        ITAMARKRECT[0] += ROUND3INC
                        global AMARKRECT
                        AMARKRECT = MARKER.get_rect(center=(ITAMARKRECT[0], ITAMARKRECT[1]))
                    elif (ROUND == 4):
                        ITAMARKRECT[0] += ROUND4INC
                        global AMARKRECT
                        AMARKRECT = MARKER.get_rect(center=(ITAMARKRECT[0], ITAMARKRECT[1]))
                    elif (ROUND == 5):
                        ITAMARKRECT[0] += ROUND5INC
                        global AMARKRECT
                        AMARKRECT = MARKER.get_rect(center=(ITAMARKRECT[0], ITAMARKRECT[1]))
                    else:
                        pass

                elif (event.key == K_0):
                    CLICKER_SOVIET += 1

                    if (ROUND == 1):
                        ITUMARKRECT[0] += ROUND1INC
                        global UMARKRECT
                        UMARKRECT = MARKER.get_rect(center=(ITUMARKRECT[0], ITUMARKRECT[1]))
                    elif (ROUND == 2):
                        ITUMARKRECT[0] += ROUND2INC
                        global UMARKRECT
                        UMARKRECT = MARKER.get_rect(center=(ITUMARKRECT[0], ITUMARKRECT[1]))
                    elif (ROUND == 3):
                        ITUMARKRECT[0] += ROUND3INC
                        global UMARKRECT
                        UMARKRECT = MARKER.get_rect(center=(ITUMARKRECT[0], ITUMARKRECT[1]))
                    elif (ROUND == 4):
                        ITUMARKRECT[0] += ROUND4INC
                        global UMARKRECT
                        UMARKRECT = MARKER.get_rect(center=(ITUMARKRECT[0], ITUMARKRECT[1]))
                    elif (ROUND == 5):
                        ITUMARKRECT[0] += ROUND5INC
                        global UMARKRECT
                        UMARKRECT = MARKER.get_rect(center=(ITUMARKRECT[0], ITUMARKRECT[1]))
                    else:
                        pass
                showProgress(CLICKER_SOVIET, CLICKER_AMERICA, ROUND)
                checkForWinner(CLICKER_SOVIET, CLICKER_AMERICA, WINREQUIREMENT)



        pygame.display.update()
        pygame.display.flip()



def terminate():
    pygame.quit()
    sys.exit()


def checkForQuit(gameover):
    for event in pygame.event.get(QUIT):
        terminate()

    if gameover == True:
        terminate()
    else:
        pass

def checkForWinner(csoviet, camerica, winreq):
    if csoviet == winreq:
        showWinner("Soviet Union", ROUND)
    elif camerica == winreq:
        showWinner("America", ROUND)
    else:
        pass

def showWinner(winner, numround):
    SURFACE.fill(BGCOLOR)



    winnerTextString = "Congradulations, " + winner + ", you won round " + str(numround)
    if (winner == "America"):
        winnerText = FONT.render(winnerTextString, True, BLACK)
        SURFACE.blit(AMSPRITE, AMRECT)
    else:
        winnerText = FONT.render(winnerTextString, True, BLACK)
        SURFACE.blit(SOVSPRITE, SOVRECT)

    winnerTextRect = winnerText.get_rect(center=(WINDOWWIDTH/2, WINDOWHEIGHT/2 + 150))
    SURFACE.blit(winnerText, winnerTextRect)
    pygame.display.update()
    pygame.display.flip()
    pygame.time.wait(1000)

    showRoundExpl(numround)

    global CLICKER_SOVIET
    global CLICKER_AMERICA
    global ROUND
    global WINREQUIREMENT
    global AMARKRECT
    global ITAMARKRECT
    global UMARKRECT
    global ITUMARKRECT
    CLICKER_AMERICA = 0
    CLICKER_SOVIET = 0
    ROUND += 1
    ITAMARKRECT = [WINDOWWIDTH/2 - 212, WINDOWHEIGHT/2 - 200]
    AMARKRECT = MARKER.get_rect(center=(ITAMARKRECT[0], ITAMARKRECT[1]))
    ITUMARKRECT = [WINDOWWIDTH/2 - 212, WINDOWHEIGHT/2 + 200]
    UMARKRECT = MARKER.get_rect(center=(ITUMARKRECT[0], ITUMARKRECT[1]))
    pygame.event.clear()
    updateWinrequirement(WINREQUIREMENT)

def updateWinrequirement(oldwin):
    global WINREQUIREMENT
    WINREQUIREMENT = oldwin + 20
    return WINREQUIREMENT


def showRoundExpl(round):
    if round == 1:
        SURFACE.fill(BGCOLOR)
        SURFACE.blit(SPUTNIKEXPL, SPUTNIKEXPLRECT)
    elif round == 2:
        SURFACE.fill(BGCOLOR)
        SURFACE.blit(LAIKA, LAIKARECT)
    elif round == 3:
        SURFACE.fill(BGCOLOR)
        SURFACE.blit(LIVING, LIVINGRECT)
    elif round == 4:
        SURFACE.fill(BGCOLOR)
        SURFACE.blit(GAGARINEXPL, GAGARINRECT)

    elif round == 5:
        SURFACE.fill(BGCOLOR)
        SURFACE.blit(MOONEXPL, MOONEXPLRECT)
        pygame.event.clear()
        pygame.display.update()
        pygame.display.flip()
        pygame.time.wait(60000)
        resetTheGame()
    else:
        pass

    PRESSTOCONT = FONT.render("Press Enter to Continue, after reading aloud the above information.", True, TEXTCOLOR)
    PRESSTOCONTRECT = PRESSTOCONT.get_rect(center=(WINDOWWIDTH/2, WINDOWHEIGHT/2 + 270))
    SURFACE.blit(PRESSTOCONT, PRESSTOCONTRECT)
    pygame.display.update()
    pygame.display.flip()
    pygame.time.wait(60000)


def showProgress(soviets, americans, waypoint):
    SURFACE.fill(BGCOLOR)
    SURFACE.blit(BGSPRITE, BGRECT)
    SURFACE.blit(PROGRESSBAR, APROGRECT)
    SURFACE.blit(PROGRESSBAR, UPROGRECT)
    SURFACE.blit(MARKER, AMARKRECT)
    SURFACE.blit(MARKER, UMARKRECT)

    if (waypoint == 1):
        SURFACE.blit(SPUTNIK, SPUTRECT)
        waypoint1texttop = FONT.render("ROUND 1", True, GREEN)
        waypoint1textbot = FONT.render("The First Satellite In Space", True, TEXTCOLOR)
        waypoint1texttoprect = waypoint1texttop.get_rect(center=TEXTOP)
        waypoint1textbotrect = waypoint1textbot.get_rect(center=TEXTBOT)
        SURFACE.blit(waypoint1texttop, waypoint1texttoprect)
        SURFACE.blit(waypoint1textbot, waypoint1textbotrect)
    elif (waypoint == 2):
        SURFACE.blit(DOG, DOGRECT)
        waypoint2texttop = FONT.render("ROUND 2", True, GREEN)
        waypoint2textbot = FONT.render("The First Dog in Space", True, TEXTCOLOR)
        wp2trt = waypoint2texttop.get_rect(center=TEXTOP)
        wp2trb = waypoint2textbot.get_rect(center=TEXTBOT)
        SURFACE.blit(waypoint2texttop, wp2trt)
        SURFACE.blit(waypoint2textbot, wp2trb)
    elif (waypoint == 3):
        SURFACE.blit(RETURN, RETURNRECT)
        wp3tt = FONT.render("ROUND 3", True, GREEN)
        wp3tb = FONT.render("The First Things Returned From Space Alive", True, TEXTCOLOR)
        wp3trt = wp3tt.get_rect(center=TEXTOP)
        wp3trb = wp3tb.get_rect(center=TEXTBOT)
        SURFACE.blit(wp3tt, wp3trt)
        SURFACE.blit(wp3tb, wp3trb)
    elif (waypoint == 4):
        SURFACE.blit(MAN, MANRECT)
        wp4tt = FONT.render("ROUND 4", True, GREEN)
        wp4tb = FONT.render("The First Man in Space", True, TEXTCOLOR)
        wp4trt = wp4tt.get_rect(center=TEXTOP)
        wp4trb = wp4tb.get_rect(center=TEXTBOT)
        SURFACE.blit(wp4tt, wp4trt)
        SURFACE.blit(wp4tb, wp4trb)
    else:
        SURFACE.blit(MOON, MOONRECT)
        wp5tt = FONT.render("ROUND 5", True, GREEN)
        wp5tb = FONT.render("The First Man on the Moon", True, TEXTCOLOR)
        tp5trt = wp5tt.get_rect(center=TEXTOP)
        tp5trb = wp5tb.get_rect(center=TEXTBOT)
        SURFACE.blit(wp5tt, tp5trt)
        SURFACE.blit(wp5tb, tp5trb)

    sovietTextString = "Soviets: " + str(soviets)
    americanTextString = "Americans: " + str(americans)

    sovietprogress = FONT.render(sovietTextString, True, TEXTCOLOR)
    americanprogress = FONT.render(americanTextString, True, TEXTCOLOR)
    sovietprogressrect = sovietprogress.get_rect(center=(WINDOWWIDTH/2, WINDOWHEIGHT/2 + 250))
    americanprogressrect = americanprogress.get_rect(center=(WINDOWWIDTH/2, WINDOWHEIGHT/2 - 250))

    SURFACE.blit(sovietprogress, sovietprogressrect)
    SURFACE.blit(americanprogress, americanprogressrect)
    pygame.display.update()
    pygame.display.flip()


def drawInitialBoard():
    SURFACE.fill(BGCOLOR)
    SURFACE.blit(BGSPRITE, BGRECT)
    pygame.display.flip()



def showTitleScreen():
    t1 = FONT.render("Coldwar Clicker", True, TEXTCOLOR)
    t1rect = t1.get_rect(center=(WINDOWWIDTH/2, WINDOWHEIGHT/2))

    SURFACE.fill(BGCOLOR)
    SURFACE.blit(BGSPRITE, BGRECT)

    SURFACE.blit(t1, t1rect)
    pygame.display.update()
    pygame.display.flip()


def resetTheGame():
    global UMARKRECT
    global AMARKRECT
    global ITAMARKRECT
    global ITUMARKRECT
    ITAMARKRECT = [WINDOWWIDTH/2 - 212, WINDOWHEIGHT/2 - 200]
    AMARKRECT = MARKER.get_rect(center=(ITAMARKRECT[0], ITAMARKRECT[1]))
    ITUMARKRECT = [WINDOWWIDTH/2 - 212, WINDOWHEIGHT/2 + 200]
    UMARKRECT = MARKER.get_rect(center=(ITUMARKRECT[0], ITUMARKRECT[1]))
    main()

if __name__ == '__main__':
    main()
